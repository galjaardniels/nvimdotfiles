local lsp = require("lsp-zero")

lsp.preset("recommended")

lsp.ensure_installed({
	'rust_analyzer',
	'sumneko_lua',
	'gopls',
	'sqls',
})

lsp.setup()
